import { Component } from "react";
import ApiService from '../data/apiService';
import {Button } from '@material-ui/core';

class NewComment extends Component{
    constructor(props){
        super(props);
        this.state = {
            Name : "",
            Comment: ""
        };

        this.handleChange  = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) 
    {    
        const target = event.target;
        const value = target.type ==='checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        })
    }

    handleSubmit(event) {
        ApiService.createComment(this.state, this.props.locationId);
        event.preventDefault();
        this.props.closeCommentForm(this.state);
    }

    render(){
        return(
             <form onSubmit={this.handleSubmit}>
                <label>Naam:</label>
                <input type="text" 
                name="Name" 
                required="required"
                value={this.state.Name}
                onChange={this.handleChange} />

                <label>Commentaar:</label>
                <input type="text" 
                name="Comment" 
                required="required"
                value={this.state.Comment}
                onChange={this.handleChange} />

                <Button type="submit">bevestig</Button>
            </form>
        );
    }
}

export default NewComment;