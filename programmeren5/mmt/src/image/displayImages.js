import { Component } from "react";
import {
    ImageList,
    ImageListItem
} from "@material-ui/core";


class DisplayImages extends Component{
    render(){
        return(
            <div>
                <ImageList sx={{ height: 300 }} cols={4} rowHeight={150}>
                    {this.props.img.map((item) => (
                        <ImageListItem key={item.link}>
                        <img
                            srcSet={`${item.link}?w=164&h=164&fit=crop&auto=format 1x,
                                ${item.link}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                            alt=""
                            loading="lazy"
                        />
                        </ImageListItem>
                    ))}
                </ImageList>
            </div>
        )
    }
}

export default DisplayImages;