import { Component } from "react";
import ApiService from '../data/apiService';
import {Button } from '@material-ui/core';

class NewImage extends Component{
    constructor(props){
        super(props);
        this.state = {
            ImageFile : null,
            Link:""
        };

        this.handleChange  = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {    
        if (event.target.files && event.target.files[0]) {
            let ImageFile = event.target.files[0];
            let ImageSrc =  URL.createObjectURL(ImageFile);
            this.setState({
                ImageFile,
                Link: ImageSrc
            });
        }
    }

    handleSubmit(event) {
        event.preventDefault();

        let formData = new FormData();
        formData.append("ImageFile", this.state.ImageFile);

        ApiService.createImage(formData, this.props.locationId);
        
        this.props.closeCommentForm(this.state);
    }

    render(){
        return(
            <div>
                
             <form onSubmit={this.handleSubmit}>

                <label>Foto:</label>
                <input type="file" 
                acceptedFiles={['image/*']}
                name="localImage" 
                required="required"
                onChange={this.handleChange} />

                <Button type="submit">bevestig</Button>
            </form>
            <img src={this.state.Link} alt=""  />
            </div>
        );
    }
}

export default NewImage;