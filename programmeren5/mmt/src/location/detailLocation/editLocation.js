import { Component } from "react";
import ApiService from '../../data/apiService';
import {Button } from '@material-ui/core';

class EditLocationForm extends Component{
    
    constructor(props){
        super(props);
        this.state = {
            Id: this.props.id,
            Name: this.props.name,
            Adress: this.props.adress,
            Price : this.props.price,
        };
console.log(this.props);
        this.handleChange  = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) 
    {    
        const target = event.target;
        const value = target.type ==='checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState(
            {
                [name]: value
            }
        )
        console.log(this.state);
    }

    handleSubmit(event) {
        event.preventDefault();
        ApiService.updateLocation(this.state, this.state.Id);
        
        this.props.handleEdit(this.state);
    }

    render(){
        return(
             <form onSubmit={this.handleSubmit}>
                <label>Naam:</label>
                <input type="text" 
                    name="Name" 
                    required="required"
                    value={this.state.Name}
                    onChange={this.handleChange} />

                <label>Adres:</label>
                <input type="text" 
                    name="Adress" 
                    required="required"
                    value={this.state.Adress}
                    onChange={this.handleChange} />

                <label>Prijs:</label>
                <input 
                    name="Price" 
                    type="text"
                    value={this.state.Price}
                    onChange={this.handleChange} />
                
                <Button type="submit">submit</Button>
            </form>
        );
    }
}

export default EditLocationForm;