import { Component} from "react";
import ApiService from '../../data/apiService';
import {Button } from '@material-ui/core';
import {withRouter} from 'react-router-dom';

class DeleteLocation extends Component{
    constructor(props){
        super(props);

        this.handleCancel = this.handleCancel.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete() {
        ApiService.deleteLocation(this.props.id);
        console.log("jklbh");
        
        this.props.history.push('/location');
      }

    handleCancel(){
        this.props.closeDeleteForm();
    }


    render(){
        return(    
            <div>
                Bent u zeker dat u "{this.props.name}" wilt verwijderen? <br/>
                <Button onClick={this.handleDelete}>Ja</Button>
                <Button onClick={this.handleCancel}>Nee</Button>
            </div>
        );
    }
}

export default withRouter(DeleteLocation);