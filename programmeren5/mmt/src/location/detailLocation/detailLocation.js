import { Component} from "react";

import ApiService from '../../data/apiService';

import EditLocationForm from './editLocation';
import DeleteLocation from "./deleteLocation";

import style from './detail.module.css';

import {
    Button,
    Backdrop
} from '@material-ui/core';
import {
    AddAPhoto,
    AddComment,
    Edit,
    DeleteForever,
    HighlightOff
} from '@material-ui/icons';

import NewComment from "../../comment/newComment";
import NewImage from "../../image/newImage";
import DisplayImages from "../../image/displayImages";
import Rating from '../../rating/rating';



class DetailLocation extends Component{
    constructor(props){
        super(props)

        this.handleEditForm = this.handleEditForm.bind(this);
    }
    state = {
        location: {

        },
        openEditForm: false,
        openDelete: false,
        openCommentForm: false,
        openImageForm: false
    }

    handleEditForm(edit){
        this.setState({openEditForm: false}) ;
        this.setState({
            location: {
                "id": edit.Id,
                "name": edit.Name,
                "adress": edit.Adress,
                "price": edit.Price,
                "ratings":this.state.location.ratings,
                "comments":this.state.location.comments,
                "images":this.state.location.images
            }
        });
    }

    openEditForm(){
        this.setState({openEditForm: true}) ;
    }

    closeEditForm(){
        this.setState({openEditForm: false}) ;
    }

    openDelete(){
        this.setState({openDelete: true});
    }

    closeDelete(){
        this.setState({openDelete: false});
    }

    openComment(){
        this.setState({openCommentForm: true});
    }

    closeComment(){
        this.setState({openCommentForm: false});
    }

    handleCommentForm(comment){
        this.setState({openCommentForm: false});

        let add = {
            "name": comment.Name,
            "comment" : comment.Comment
        }
        let comments = this.state.location.comments;
        comments.push(add);

        this.setState({
            location: {
                "id": this.state.location.Id,
                "name": this.state.location.Name,
                "adress": this.state.location.Adress,
                "price": this.state.location.Price,
                "ratings": this.state.location.ratings,
                "comments": comments,
                "images": this.state.location.images
            }
        });
    }

    openImageform(){
        this.setState({openImageForm: true});
    }

    closeImageform(){
        this.setState({openImageForm: false});

    }

    handleImageForm(image){
        this.setState({openImageForm: false});

        let images = this.state.location.images;
        let add = {"link": image.link};
        images.push(add);        

        this.setState({
            location: {
                "id": this.state.location.Id,
                "name": this.state.location.Name,
                "adress": this.state.location.Adress,
                "price": this.state.location.Price,
                "ratings": this.state.location.ratings,
                "comments": this.state.location.comments,
                "images": this.state.location.images
            }
        });
    }

    componentDidMount(){
        ApiService.getOneLocation(this.props.id)
            .then(o => this.setState({location: o}));
    }

    render(){
        return(
            <div>
                <div className={style.detail}>
                    <div className={style.id}>{this.state.location.id}</div>
                    
                    <div className={style.name}>
                        {this.state.location.name}
                        <div className={style.icons}>
                            <Button onClick={this.openEditForm.bind(this)}>
                                <Edit />
                            </Button>
                            <Button>
                                <AddComment onClick={this.openComment.bind(this)}/>
                            </Button>
                            <Button>
                                <AddAPhoto onClick={this.openImageform.bind(this)} />
                            </Button>
                            <Button>
                                <DeleteForever onClick={this.openDelete.bind(this)}/>
                            </Button>
                        </div>
                    </div>
                    
                    <div className={style.details}>
                        <div className={style.price}>{this.state.location.price} €</div>
                        <div className={style.adress}>{this.state.location.adress}</div>
                        <Rating rating={this.state.location.ratings} locId={this.state.location.id}/>
                    </div>
                    <div>
                        {this.state.location.images !== undefined ?
                            
                            <DisplayImages img={this.state.location.images}/>
                            :<div></div> 
                            }
                    </div>

                    {this.state.location.comments !== undefined ? 
                    <div className={style.comments}>{this.state.location.comments.map(comment => 
                        <div className={style.commentGroup}>
                            <div className={style.commentName}>naam: {comment.name}</div>
                            <div className={style.comment}>{comment.comment}</div>
                        </div>)}
                    </div>
                :<div></div>}
                    
                </div>

                <Backdrop className={style.backdrop} open={this.state.openEditForm}>
                    <Button className={style.close}  onClick={this.closeEditForm.bind(this)}>
                        <HighlightOff/>
                    </Button>
                    {this.state.location.id !== 0 ?
                            <div className={style.contentBackdrop}>
                                {this.state.location.name !== undefined ?
                                <EditLocationForm  
                                    handleEdit ={this.handleEditForm.bind(this)}
                                    {...console.log(this.state.location.id)
                                }
                                    id={this.state.location.id}
                                    name={this.state.location.name}
                                    adress={this.state.location.adress} 
                                    price={this.state.location.price}
                                /> : null}
                            </div>
                        :<div></div>}
                </Backdrop>

                <Backdrop className={style.backdrop} open={this.state.openDelete}>
                    <Button className={style.close}  onClick={this.closeDelete.bind(this)}>
                        <HighlightOff/>
                    </Button>
                    {this.state.location.id !== 0 ?
                            <div className={style.contentBackdrop}>
                                <DeleteLocation className={style.contentBackdrop}
                                    closeDeleteForm ={this.closeDelete.bind(this)}
                                    id={this.state.location.id}
                                    name={this.state.location.name}
                                />
                            </div>
                        :null}
                </Backdrop>        

                <Backdrop className={style.backdrop} open={this.state.openCommentForm}>
                    <Button className={style.close}  onClick={this.closeComment.bind(this)}>
                        <HighlightOff/>
                    </Button>
                    {this.state.location.id !== 0 ?
                            <div className={style.contentBackdrop}>
                                <div>Schrijf hier u commentaar op {this.state.location.name}.</div>
                                <NewComment className={style.contentBackdrop}
                                    closeCommentForm ={this.handleCommentForm.bind(this)}
                                    locationId = {this.state.location.id}
                                />
                            </div>
                        : null}
                </Backdrop>       

                <Backdrop className={style.backdrop} open={this.state.openImageForm}>
                    <Button className={style.close}  onClick={this.closeImageform.bind(this)}>
                        <HighlightOff/>
                    </Button>
                    {this.state.location.id !== 0 ?
                            <div className={style.contentBackdrop}>
                                <div>Voer u afbeelding in dat hoort bij {this.state.location.name}.</div>
                                <NewImage className={style.contentBackdrop}
                                    closeCommentForm ={this.handleImageForm.bind(this)}
                                    locationId = {this.state.location.id}
                                />
                            </div>
                        :<div></div>}
                </Backdrop>    
            </div>
        )
        
    }
}

export default DetailLocation;