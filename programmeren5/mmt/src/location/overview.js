import { Component } from "react";
import ApiService from '../data/apiService';
import NewLocation from "./newLocation";
import {
    ImageList,
    ImageListItem,
    ListSubheader,
    ImageListItemBar,
    Link
} from '@material-ui/core';


class Overview extends Component{
    constructor(props){
        super(props);
        this.state = {
            locations: []
        };

        this.handleNewLocation = this.handleNewLocation.bind(this);
    }

    handleNewLocation(location){
        let locations = this.state.locations
        location.then(result => locations.push(result));

        this.setState({
            locations: locations
        });
        console.log(this.state.locations);
    }
    

    componentDidMount(){
        ApiService.getLocations()
            .then(o => this.setState({locations: o}));
    }

    render(){
        return(
            <div>
                <NewLocation handleNewLocation={this.handleNewLocation.bind(this)} />
   
                <ImageList sx={{height: 450 }}>
                    <ImageListItem key="Subheader" cols={4}>
                        <ListSubheader component="div">Lijst van locaties</ListSubheader>
                    </ImageListItem>
                    {this.state.locations.map(item =>
                        <ImageListItem key={item.name}>
                            {(item.images !== null ) && (item.images[0] !== undefined)?
                                <img
                                srcSet={`${item.images[0].link}?w=248&fit=crop&auto=format 1x,
                                    ${item.images[0].link}?w=248&fit=crop&auto=format&dpr=2 2x`}
                                alt={item.Name}
                                loading="lazy"
                            />
                                :<div style={{minHeight: "50px", minWidth: "137px"}}></div>
                            }
                            <Link href={"location/" + item.id} underline="hover">
                                <ImageListItemBar
                                    title={item.name}
                                    subtitle={item.adres}
                                />
                            </Link>
                        </ImageListItem>
                    )}
                </ImageList>
            </div>
        );
    }
}

export default Overview;