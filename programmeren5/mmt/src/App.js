import React from 'react';
import { BrowserRouter as Router, Switch, Route, useParams } from 'react-router-dom';

import DetailLocation from './location/detailLocation/detailLocation';
import Overview from './location/overview';

import './App.css';

function App() {

  

  return (
    
    <div className="App">
      <Router>
          <Switch>
              
              <Route path="/location"  component={Overview} >
                <Route path={`/location/:locId`} component={GetDetailLocation} />
                <Route exact path="/location"  component={Overview} />
            </Route>
            <Route exact path="" component={Overview} />
          </Switch>
      </Router>
    </div>
  );
}


function GetDetailLocation() {
  let { locId } = useParams();
  
  return (
      <DetailLocation id={locId} />
  );
}


export default App;
