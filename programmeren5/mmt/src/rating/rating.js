import { Component } from "react";
import ApiService from '../data/apiService';
import {Button } from '@material-ui/core';
import {
    ThumbUp,
    ThumbDown,
} from '@material-ui/icons';
import style from './rating.module.css';

class Rating extends Component{
    constructor(props){
        super(props);
        this.state = {
            goodRating: (this.props.rating === undefined ? 0 : this.props.rating.numberLikes),
            badRating: (this.props.rating === undefined ? 0 : this.props.rating.numberDislikes)
        };
        
        this.handleRating = this.handleRating.bind(this);
    }

    handleGoodRating(){
        this.handleRating(true);
    }

    handleBadRating(){
        this.handleRating(false);
    }

    handleRating(goodRate){
        let good = this.state.goodRating;
        let bad = this.state.badRating;
        
        goodRate ? good++ : bad++;

        this.setState({
            goodRating: good,
            badRating: bad
        })
        
        let rate= {goodRate};
        ApiService.createRating(this.props.locId, rate);
    }

    render(){
        return(
             <div className={style.rating}>
                <Button classNames={style.badButton}>
                    <ThumbDown onClick={this.handleBadRating.bind(this)} color={'inherit'}/>
                </Button>
                <Button>
                    <ThumbUp onClick={this.handleGoodRating.bind(this)} color={'inherit'}/>
                </Button>
                <div>
                    <div className={style.bad}>{
                        this.state.badRating
                        }
                    </div>
                    <div className={style.good}>
                        {this.state.goodRating }
                    </div>
                </div>
             </div>
        );
    }
}

export default Rating;