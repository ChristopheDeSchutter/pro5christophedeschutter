const baseUrl = 'https://localhost:5001/api/';

async function getLocations(){return load('location')};
async function getOneLocation(id = 0){return load('location/' + id)};
async function createLocation(data = []){return await post('location', data)};
async function updateLocation(data = [], id){return put('location/' + id, data)}
async function deleteLocation(id){console.log(id);return deleteApi('location/' + id)}

async function createComment(data = [], locId){
    return post('location/'+ locId + '/comment', data)};

async function createImage(data = [], locId){
    return postFile('location/'+ locId + '/image', data)};

async function getRating(locId){return load('location/'+ locId + '/rating')}
async function createRating(locId, rate){return post('location/'+ locId + '/rating', rate)}

const load = async function(url = ''){
    let response = await fetch(baseUrl + url, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
    })
    .then(r => r.json());
    
    return response;
}

const deleteApi = async function(url = ''){
    let response = await fetch(baseUrl + url, {
        method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
    })
    .then(r => r.json());
    
    return response;
}

const postFile = async function(url = '', data = []){
    // Default options are marked with *
    const response = await fetch(baseUrl + url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        redirect: 'follow',
        body: data // body data type must match "Content-Type" header
    });
    return response.json(); // parses JSON response into native JavaScript objects
}

const post = async function(url = '', data = []){
        // Default options are marked with *
        const response = await fetch(baseUrl + url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'omit', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        })
        .then(r => r.json());
        return response; // parses JSON response into native JavaScript objects
}

const put = async function(url = '', data = []){
    // Default options are marked with *
    const response = await fetch(baseUrl + url, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'omit', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    return response.json(); // parses JSON response into native JavaScript objects
}

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    getLocations,
    getOneLocation,
    createLocation,
    updateLocation,
    deleteLocation,
    createComment,
    createImage,
    getRating,
    createRating
};