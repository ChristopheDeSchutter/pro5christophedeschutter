﻿using mmt_back_end.Models;
using static System.Net.Mime.MediaTypeNames;

namespace mmt_back_end.Helper
{
    public class ImageLink
    {

        public Images GetImageLink(Images image)
        {
            image.Link = "https://localhost:5001/images/" + image.Link;

            return image;
        }
    }
}
