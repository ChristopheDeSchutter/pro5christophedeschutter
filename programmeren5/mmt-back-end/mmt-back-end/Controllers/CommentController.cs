﻿using Microsoft.AspNetCore.Mvc;
using mmt_back_end.Map;
using mmt_back_end.Models;
using mmt_back_end.Models.ModelsMap;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace mmt_back_end.Controllers
{
    [Route("api/Location/{locationId}/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly MmtContext context;

        public CommentController(MmtContext _context)
        {
            context = _context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Comments>>> GetComments(long locationId)
        {
            List<Comments> comments = await context.Comments
                .Include(loc => loc.Location)
                .Where(loc => loc.Location.Id == locationId)
                .ToListAsync();

            return Ok(comments);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Comments>> GetComment(long locationId, long id)
        {
            Comments comment = await context.Comments
                .Include(loc => loc.Location)
                .Where(loc => loc.Location.Id == locationId)
                .SingleOrDefaultAsync(com => com.Id ==id);

            return comment;
        }

        [HttpPost]
        public async Task<ActionResult<Comments>> PostComment(long locationId, [FromBody] CommentNoLocation comment)
        {
            Locations location = await context.Locations.FindAsync(locationId);

            var map = new CommentsMap();
            Comments commentDb = map.Map(comment, location);

            context.Comments.Add(commentDb);
            await this.context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetComment), new {locationId, id = commentDb.Id}, comment);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Comments>> PutComment(long locationId, long id, [FromBody] CommentNoLocation comment)
        {
            if (id != comment.Id)
            {
                return BadRequest();
            }

            Locations location = await context.Locations.FindAsync(locationId);

            var map = new CommentsMap();
            Comments commentDb = map.Map(comment, location);

            context.Entry(commentDb).State = EntityState.Modified;
            await context.SaveChangesAsync();

            return Ok(commentDb);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Comments>> DeleteComment(long locationId, long id)
        {
            Comments comment = await context.Comments
                .Include(loc => loc.Location)
                .SingleOrDefaultAsync(com => com.Id == id);

            if(comment == null || locationId != comment.Location.Id)
            {
                return BadRequest();
            }

            context.Comments.Remove(comment);
            await context.SaveChangesAsync();

            return NoContent();
        }
    }
}
