﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using mmt_back_end.Models;
using mmt_back_end.Models.ModelsMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mmt_back_end.Controllers
{
    [Route("api/Location/{locationId}/[controller]")]
    [ApiController]
    public class RatingController : ControllerBase
    {
        private readonly MmtContext context;

        public RatingController(MmtContext _context)
        {
            context = _context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Rating>>> GetRating(long locationId)
        {
            List<Rating> rating = await context.Ratings
                .Where(loc => loc.Location.Id == locationId)
                .ToListAsync();

            return Ok(rating);
        }

        // PUT api/<LocationController>/5
        [HttpPost]
        public async Task<ActionResult<Rating>> PostRating(
            long locationId, [FromBody] Rate rate)
        {
            
            Rating rating = await context.Ratings
                .SingleOrDefaultAsync(loc => loc.Location.Id == locationId);

            if(rating == null)
            {
                rating = new Rating();
                Locations location = await context.Locations.FindAsync(locationId);

                rating.Location = location;
                context.Ratings.Add(rating);
                await context.SaveChangesAsync();
            }

            if (rate.goodRate)
            {
                rating.NumberLikes ++;
            }
            else
            {
                rating.NumberDislikes++;
            }

            context.Entry(rating).State = EntityState.Modified;
            await context.SaveChangesAsync();

            return Ok(rating);
        }
    }
}
