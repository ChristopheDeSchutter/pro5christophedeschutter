﻿using Microsoft.AspNetCore.Mvc;
using mmt_back_end.Map;
using mmt_back_end.Models;
using mmt_back_end.Models.ModelsMap;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using mmt_back_end.Helper;

namespace mmt_back_end.Controllers
{
    [Route("api/Location/{locationId}/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private readonly MmtContext context;
        private readonly IWebHostEnvironment environment;

        public ImageController(MmtContext context, IWebHostEnvironment environment)
        {
            this.context = context;
            this.environment = environment;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Images>>> GetImages(long locationId)
        {
            List<Images> images = await context.Images
                .Include(loc => loc.Location)
                .Where(loc => loc.Location.Id == locationId)
                .ToListAsync();

            foreach (Images image in images)
            {
                ImageLink helper = new ImageLink();
                Images temp = helper.GetImageLink(image);
                image.Link = temp.Link;
            }

            return Ok(images);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Images>> GetImage(long locationId, long id)
        {
            Images image = await context.Images
                .Include(loc => loc.Location)
                .SingleOrDefaultAsync(img => img.Id == id);
           
            ImageLink helper = new ImageLink();
            image = helper.GetImageLink(image);

            return image;
        }

        [HttpPost]
        public async Task<ActionResult<Images>> PostImage(long locationId,[FromForm] ImageNoLocation image)
        {
            Locations location = await context.Locations.FindAsync(locationId);

            image.Link = await SaveImage(image.ImageFile);

            var map = new ImagesMap();
            Images imageDb = map.Map(image, location);

            context.Images.Add(imageDb);
            await context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetImage), new { locationId, id = imageDb.Id }, image);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Images>> DeleteImage(long locationId, long id)
        {
            Images image = await context.Images
                .Include(loc => loc.Location)
                .SingleOrDefaultAsync(img => img.Id == id);

            if (image == null || locationId != image.Location.Id)
            {
                return BadRequest();
            }

            context.Images.Remove(image);
            await context.SaveChangesAsync();

            return NoContent();
        }

        [NonAction]
        public async Task<String> SaveImage(IFormFile imageFile)
        {
            string imageName = new String(Path.GetFileNameWithoutExtension(imageFile.FileName).Take(10).ToArray()).Replace(' ', '_');
            imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(imageFile.FileName);
            var imagePath = Path.Combine(environment.ContentRootPath, "images", imageName);
            using(var fileStream = new FileStream(imagePath,FileMode.Create))
            {
                await imageFile.CopyToAsync(fileStream);
            }

            return imageName;
        }
    }
}
