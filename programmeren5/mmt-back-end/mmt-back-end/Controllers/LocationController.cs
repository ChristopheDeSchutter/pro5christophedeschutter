﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using mmt_back_end.Helper;
using mmt_back_end.Models;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace mmt_back_end.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationController : ControllerBase
    {
        private readonly MmtContext context;

        public LocationController(MmtContext _context)
        {
            context = _context;
        }

        // GET: api/<LocationController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Locations>>> GetLocations()
        {
            var locations = await context.Locations
                .Include(com => com.Comments)
                .Include(img => img.Images)
                .ToListAsync();

            foreach ( Locations location in locations)
            {
                foreach (Images image in location.Images)
                {
                    ImageLink helper = new ImageLink();
                    Images temp = helper.GetImageLink(image);
                    image.Link = temp.Link;
                }
            }

            return Ok(locations);
        }

        // GET api/<LocationController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Locations>> GetLocation(long id)
        {
            var location = await context.Locations
                .Include(com => com.Comments)
                .Include(img => img.Images)
                .Include(rate => rate.Ratings)
                .SingleOrDefaultAsync(loc => loc.Id == id);

            if (location == null)
            {
                return NotFound();
            }

            foreach (Images image in location.Images)
            {
                ImageLink helper = new ImageLink();
                Images temp = helper.GetImageLink(image);
                image.Link = temp.Link;
            }

            return Ok(location);
         }

        // POST api/<LocationController>
        [HttpPost]
        public async Task<ActionResult<Locations>>PostLocation([FromBody] Locations value)
        {
            context.Locations.Add(value);
            await context.SaveChangesAsync();

            return Ok(value);
        }

        // PUT api/<LocationController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult<Locations>> PutLocation(int id, [FromBody] Locations value)
        {
            if(id != value.Id)
            {
                return BadRequest();
            }

            context.Entry(value).State = EntityState.Modified;
            await context.SaveChangesAsync();

            return Ok(value);
        }

        // DELETE api/<LocationController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Locations>> DeleteLocation(long id)
        {
            Locations locatie = await context.Locations.FindAsync(id);

            if(locatie == null)
            {
                return BadRequest();
            }

            context.Locations.Remove(locatie);
            await context.SaveChangesAsync();

            return NoContent();
        }
    }
}
