﻿using mmt_back_end.Models;
using mmt_back_end.Models.ModelsMap;

namespace mmt_back_end.Map
{
    public class CommentsMap
    {
        public Comments Map(CommentNoLocation comment, Locations location)
        {
            Comments commentDb = new Comments
            {
                Id = comment.Id,
                Name = comment.Name,
                Comment = comment.Comment,
                Location = location
            };

            return commentDb;
        }
    }
}
