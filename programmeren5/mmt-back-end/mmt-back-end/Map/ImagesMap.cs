﻿using mmt_back_end.Models;
using mmt_back_end.Models.ModelsMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mmt_back_end.Map
{
    public class ImagesMap
    {
        public Images Map(ImageNoLocation imageNoLoc, Locations location)
        {
            Images imageDb = new Images
            {
                Id = imageNoLoc.Id,
                Link = imageNoLoc.Link,
                Location = location
            };

            return imageDb;
        }
    }
}
