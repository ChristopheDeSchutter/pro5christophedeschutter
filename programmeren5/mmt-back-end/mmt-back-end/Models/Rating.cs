﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace mmt_back_end.Models
{
    public class Rating
    {
        public Rating()
        {
            NumberLikes = 0;
            NumberDislikes = 0;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public int NumberLikes { get; set; } 
        public int NumberDislikes { get; set; } 

        [ForeignKey("LocationId")]
        [Required]
        public Locations Location { get; set; }
    }
}
