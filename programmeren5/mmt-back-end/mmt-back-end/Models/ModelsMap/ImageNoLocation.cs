﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace mmt_back_end.Models.ModelsMap
{
    public class ImageNoLocation
    {
        public long Id { get; set; }

        public string Link { get; set; }

        public IFormFile ImageFile { get; set; }
    }
}
