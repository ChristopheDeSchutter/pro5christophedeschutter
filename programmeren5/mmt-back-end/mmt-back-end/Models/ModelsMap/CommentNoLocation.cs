﻿using System.ComponentModel.DataAnnotations;

namespace mmt_back_end.Models.ModelsMap
{
    public class CommentNoLocation
    {
        public long Id { get; set; }
        public string Name { get; set; }

        [Required]
        public string Comment { get; set; }
    }
}
