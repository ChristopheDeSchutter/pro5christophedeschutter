﻿using Microsoft.EntityFrameworkCore;

namespace mmt_back_end.Models
{
    public class MmtContext : DbContext
    {
            public MmtContext(DbContextOptions<MmtContext> options)
                : base(options)
            {
            }

            public DbSet<Locations> Locations { get; set; }
            public DbSet<Images> Images { get; set; }
            public DbSet<Comments> Comments { get; set; }
            public DbSet<Rating> Ratings { get; set; }
    }
}
