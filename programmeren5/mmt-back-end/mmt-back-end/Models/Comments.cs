﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace mmt_back_end.Models
{
    public class Comments
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public string Name { get; set; }

        [Required]
        public string Comment { get; set; }

        [ForeignKey("LocationId")]
        [Required]
        public Locations Location { get; set; }
    }
}
