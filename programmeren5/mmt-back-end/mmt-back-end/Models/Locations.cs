﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace mmt_back_end.Models
{
    public class Locations
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id {get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MaxLength(200)]
        public string Adress { get; set; }

        public double Price { get; set; }

        public Rating Ratings { get; set; }
        public ICollection<Comments> Comments { get; set; }
        public ICollection<Images> Images { get; set; }

        public static implicit operator string(Locations v)
        {
            throw new NotImplementedException();
        }
    }
}
