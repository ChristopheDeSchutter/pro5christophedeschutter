import ImageSummary from '../elements/ImageSummary';
import AButton from '../elements/AButton';

function ArticleSummary(props) {
    const style = {
        display: "flex",
        flexDirection: "column",
        width: "10em"
    }
    return (<article style={style}>
        <ImageSummary name={props.name} image={props.image}/>
        <AButton caption={props.name}/>
    </article>);
}

export default ArticleSummary;