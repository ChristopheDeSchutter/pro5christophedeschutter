import React, { Component } from 'react';
import ArticleOverview from '../overview/articleOverview';
import ArticleDetail from '../detail/ArticleDetail';

class ArticleApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            overview: true
        };
    }

    showDetail = (item) => {
        this.setState({
            overview: false,
            article: item
        });
    };

    showOverview = () => {
        this.setState({
            overview: true,
            article: null
        });
    };

    render() {
        if (this.state.overview) {
            return (
                <ArticleOverview article={this.props.article} action={this.showDetail} />
            );
        } else {
            return (<ArticleDetail article={this.state.article} actionOverview={this.showOverview} />)
        }
    }
}

export default ArticleApp;