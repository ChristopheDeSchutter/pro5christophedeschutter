function LeafLetNode() {
    const style = {
        width: "40em",
        height: "40em"
    }
    return (<div id="leaflet-id" style={style}></div>)
}

export default LeafLetNode;