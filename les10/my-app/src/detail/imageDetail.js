function ImageDetail(props){
    return(
        <image src="{props.image}" alt="{props.name}"/>
    );
}

export default ImageDetail;