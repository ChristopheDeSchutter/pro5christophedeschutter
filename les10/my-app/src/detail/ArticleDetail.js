import React, { Component } from 'react';
import AButton from '../elements/AButton';
import ImageDetail from './imageDetail';
import LikePanel from './likePanel';
import LeafLetNode from './leafNode';

class ArticleDetail extends Component{
    style = {
        detail: {
            display: "flex",
            flexDirection: "column",
            width: "70%"
        },
        top: {
            display: "flex",
            flexDirection: "row"
        },
        middle: {
            display: "flex",
            flexDirection: "row"
        },
        comment: {
            display: "block"
        }
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const script = document.createElement("script");
        script.src = "https://unpkg.com/leaflet@1.6.0/dist/leaflet.js";
        script.async = true;
        script.onload = () => this.scriptLoaded();

        document.body.appendChild(script);

        this.leafLetView('leaflet-id');
    }

    showOverview = () => { this.props.actionOverview() }

/*    leafLetView = (id) => {
        // Initialiseert de mijnKaart constante
        const map = L.map(id);
        // Laad de basiskaart
        const baseMap = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        });
        // plaats de basismap in het html element met id = kaart-id
        baseMap.addTo(map);
        // Stel het gebied van de kaart in
        // de laatste parameter bepaalt de grootte van het getoonde gebied
        // hoe groter het getal, hoe gedetailleerder
        // alert(this.props.article.longitude);
        const pos1 = this.props.article.longitude.substr(0, 5);
        const pos2 = this.props.article.latitude.substr(0, 5);
        const mark = L.marker([this.props.article.latitude, this.props.article.longitude]).addTo(map);
        // alert(pos1 + pos2);
        map.setView([pos2, pos1], 12);
    }
*/
    render() {
        return (<article style={this.style}>
            <AButton caption="Terug naar overzicht" onClick={this.showOverview} />
            <div style={this.style.top}>
                <ImageDetail name={this.props.article.name} image={`images/big/${this.props.article.image}`} />
                <LeafLetNode />
            </div>
            <div style={this.style.middle}>
                <div>
                    <ul>
                        <li>{this.props.article.name}</li>
                        <li>{this.props.article.type}</li>
                        <li>{this.props.article.period}</li>
                        <li>{this.props.article.country}</li>
                        <li>{this.props.article.region}</li>
                        <li>{this.props.article.city}</li>
                    </ul>
                </div>
                <div>
                    <ul>
                        <li>{this.props.article.coordinates}</li>
                        <li>{this.props.article.longitude}</li>
                        <li>{this.props.article.latitude}</li>
                    </ul>
                    <LikePanel />
                </div>
            </div>
            <textarea style={this.style.comment}></textarea>
            <AButton caption="Verzenden" />
        </article>);
    }
}

export default ArticleDetail;