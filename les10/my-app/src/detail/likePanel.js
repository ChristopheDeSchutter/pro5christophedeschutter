import React from 'react';
import AButton from '../elements/AButton';

class LikePanel extends React.Component {

    host = "http://localhost:58013/MmtLike";

    constructor(props) {
        super(props); // Now 'this' is initialized by calling the parent constructor.
        this.state = {
            likes: 0
        };
        this.getLikes();
    }

    getLikes = () => {
        const keyValue = this.props.keyValue;
        let count = 0;
        if (typeof keyValue !== 'undefined') {
            // als de callback wordt uitgevoerd, is this niet meer in de scope
            // daarom slaan we die op in een constante en geven die met de callback mee 
            // const self = this;
            const url = `${this.host}/${keyValue}`;
            fetch(url)
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        likes: data.likes
                    });
                });
        }
    }

    postLikes = () => {
        let item = {
            Key: this.props.keyValue,
            Name: 'onbelangrijk',
            Likes: 1
        };
   /*     postData(this.host, item)
            .then(data => {
                this.setState({
                    likes: data.likes
                });
            }); */
    }

    incrementLike = () => {
        const keyValue = this.props.keyValue;
        if (typeof keyValue !== 'undefined') {
            this.postLikes();
        } else {
            let newCount = this.state.likes + 1;
            this.setState({
                likes: newCount
            });
        }
    };

    render() {
        return (
            <div>
                <AButton caption="I like" onClick={this.incrementLike} />
                <label caption={this.state.likes} />
            </div>);
    }
}
export default LikePanel;