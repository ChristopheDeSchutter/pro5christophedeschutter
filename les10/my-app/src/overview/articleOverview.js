import ArticleSummary from '../summary/ArticleSummary';

function ArticleOverview(props) {
    const style = {
        color: 'white',
        display: 'flex',
        flexWrap: 'wrap'
    };
    // alert(JSON.stringify(props));
    return (
        <main>
            <div style={style}>
                {props.article.map(item => (<ArticleSummary item={item} key={item.key} action={props.action} />))}
            </div>
        </main>
    );
}

export default ArticleOverview;