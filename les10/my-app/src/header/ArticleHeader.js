function ArticleHeader(props) {
    const style = {
        header: {
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            width: "100%",
            heigth: "10em"
        },
        title: {
            alignSelf: "center",
            fontFamily: "Arial",
            color: "red",
            paddingRight: "1em"
        },
        logo: {
            height: "10em"
        }
    }
    return (<header style={style.header}>
        <img alt={`Logo ${props.title}`} src={props.image} style={style.logo}/>
        <h1 style={style.title}>{props.title}</h1>
    </header>);
}

export default ArticleHeader;