function ImageSummary(props) {
    const style = {
        borderRadius: "6px",
    }
    return (<img style={style} alt={`afbeelding van ${props.name}`} src={props.image} />
    );
}

export default ImageSummary;