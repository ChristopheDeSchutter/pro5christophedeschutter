import React,{useEffect, useState} from 'react';
import './App.css';
import ArticleSummary from './summary/ArticleSummary';
import ArticleHeader from './header/ArticleHeader';
import ArticleApp from './detail/articleApp';

function App() {

  const [data,setData]=useState([]);
  const url = 'data/neolithicum.json';

  const getData=()=>{
    fetch(url
    ,{
      headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
       }
    }
    )
    .then(function(response){
      console.log(response)
      return response.json();
    })
    .then(function(myJson) {
      console.log(myJson);
      setData(myJson)
    });
  }
  useEffect(()=>{
    getData()
  },[])

 
  return (
    <div className="App">
      <ArticleHeader title={data.title} image={data.logoUrl} />
      <ArticleApp article={data.curiosity} />
      <ArticleSummary name="Allée couverte du Colombier" image="images/small/Allee couverte du Colombier.jpg" />
    </div>
  );
}

export default App;