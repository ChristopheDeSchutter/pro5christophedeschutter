window.onload=function(){
    canvas = document.getElementById("canvas");
    document.getElementById("stop").onclick = start;
    document.getElementById("sneller").onclick = sneller;

    ctx = canvas.getContext("2d");    
}
var x=0;
var xReverse = false;
var y=0;
var yReverse = false;
var degrees;
var canvas;
var ctx;
var clock1;
var moveCanvas;

function start(){
    console.log(this.innerHTML);
    if(this.innerHTML=="Stop de tijd")
    {
        clearInterval(clock1);  
        clearInterval(moveCanvas);
        this.innerHTML="Start de tijd";
    }else{
        moveCanvas = setInterval(move,1);
        clock1 = setInterval(digitalClock, 1);
        this.innerHTML="Stop de tijd"
    }
}


 function sneller(){
    moveCanvas = setInterval(move,1);
 }

function move(){

    if(!xReverse)
    {
        x++;
        if(x+canvas.width >= window.innerWidth)
        {
            xReverse=true;
        }
    }else{
        x--;
        if(x <= 0)
        {
            xReverse=false;
        }
    }
    
    if(!yReverse)
    {
        y++;
        if(y+canvas.height >= window.innerHeight)
        {
            yReverse=true;
        }
    }else{
        y--;
        if(y <= 0)
         {
            yReverse=false;
         }
    }
    
    canvas.style.left = x+"px";
    canvas.style.top = y+"px";
}

function digitalClock(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    var time =  new Date();
    let houres = time.getHours().toString();
    let minutes = time.getMinutes().toString();
    let seconds = time.getSeconds().toString();

    //set houres
    if(houres.length === 1)
    {
        drawZero(20);
        drawNumber(houres, 50);
    }else
    {
        drawNumber(houres.slice(0,1), 20);
        drawNumber(houres.slice(1,2), 50);
    }

    drawColon(80);
    //set Houres
    if(minutes.length === 1)
    {
        drawZero(90);
        drawNumber(minutes, 120);
    }else
    {
        drawNumber(minutes.slice(0,1), 90);
        drawNumber(minutes.slice(1,2), 120);
    }
    drawColon(160);

    //set seconds
    if(seconds.length === 1)
    {
        drawZero(170);
        drawNumber(seconds, 200);
    }else
    {
        drawNumber(seconds.slice(0,1), 170);
        drawNumber(seconds.slice(1,2), 200);
    }
}

function drawNumber(n, width){
    switch(n){
        case "0":
            drawZero(width);
            break;
        case "1":
            drawOne(width);
            break;
        case "2":
            drawTwo(width);
            break;
        case "3":
            drawThree(width);
            break;
        case "4":
            drawFour(width);
            break;
        case "5":
            drawFive(width);
            break;
        case "6":
            drawSix(width);
            break;
        case "7":
            drawSeven(width);
            break;
        case "8":
            drawEight(width);
            break;
        case "9":
            drawNine(width);
            break;
    }
}

function drawZero(x){
    ctx.beginPath();
    ctx.moveTo(15 + x, 0);
    ctx.bezierCurveTo(0 + x, 0, -10 + x, 70, 15 + x, 70);
    ctx.stroke();
    ctx.beginPath();
    ctx.moveTo(15 + x, 0);
    ctx.bezierCurveTo(30 + x, 0, 40 + x, 70, 15 + x, 70);
    ctx.stroke();
    //M 15 0 C 0 0 -10 70 15 70 M 15 0 C 30 0 40 70 15 70
}
function drawOne(x){
    ctx.beginPath();
    ctx.moveTo(25 + x, 0);
    ctx.lineTo(25 + x, 70);
    ctx.moveTo(25 + x, 0);
    ctx.lineTo(5 + x, 25);
    ctx.stroke();
    //M 25 0 L 25 70 M 25 0 L 5 25

}
function drawTwo(x){
    ctx.beginPath();
    ctx.moveTo(2 + x, 23);
    ctx.bezierCurveTo(4 + x, -23, 65 + x, 8, 0 + x, 68);
    ctx.stroke();
    ctx.beginPath();
    ctx.moveTo(0 + x, 68);
    ctx.bezierCurveTo(10 + x, 73, 17 + x, 67, 30 + x, 69);
    ctx.stroke();
    //M 2 23 C 4 -23 65 0 0 68 M 0 68 C 10 73 17 67 30 69

}
function drawThree(x){
    ctx.beginPath();
    ctx.moveTo(9 + x, 35);
    ctx.bezierCurveTo(51 + x, 13, 12 + x, -6, 2 + x, 7);
    ctx.stroke();
    ctx.beginPath();
    ctx.moveTo(9 + x, 35);
    ctx.bezierCurveTo(40 + x, 36, 34 + x, 79, 2 + x, 68);
    ctx.stroke();
    //M 9 35 C 51 13 12 -6 2 7 M 9 35 C 40 36 34 79 2 68
}
function drawFour(x){
    ctx.beginPath();
    ctx.moveTo(23 + x, 0);
    ctx.lineTo(0 + x, 43);
    ctx.moveTo(0 + x, 43);
    ctx.lineTo(30 + x, 43);
    ctx.moveTo(23 + x, 0);
    ctx.lineTo(23 + x, 70);
    ctx.stroke();
    //M 23 0 L 0 43 M 0 43 L 30 43 M 23 0 L 23 70
}
function drawFive(x){
    ctx.beginPath();
    ctx.moveTo(30 + x, 0);
    ctx.lineTo(0 + x, 0);
    ctx.moveTo(0 + x, 0);
    ctx.lineTo(0 + x, 30);
    ctx.moveTo(0 + x, 30);
    ctx.bezierCurveTo(47 + x, 8, 30 + x, 84, 2 + x, 67);
    ctx.stroke();
    //M 30 0 L 0 0 M 0 0 L 0 26 M 0 26 C 47 8 30 86 2 67
}

function drawSix(x){
    ctx.beginPath();
    ctx.moveTo(13 + x, 30);
    ctx.bezierCurveTo(71 + x, 63, -53 + x, 110, 30 + x, 0);
    ctx.stroke();
    //M 13 30 C 71 63 -53 110 30 0
}

function drawSeven(x){
    ctx.beginPath();
    ctx.moveTo(31 + x, 0);
    ctx.lineTo(6 + x, 70);
    ctx.moveTo(31 + x, 0);
    ctx.lineTo(0 + x, 0);
    ctx.stroke();
    //M 31 0 L 6 70 M 31 0 L 0 0
}

function drawEight(x){
    ctx.beginPath();
    ctx.moveTo(15 + x, 0);
    ctx.bezierCurveTo(64 + x, 2, -32 + x, 68, 15 + x, 70);
    ctx.stroke();
    ctx.beginPath();
    ctx.moveTo(15 + x, 0);
    ctx.bezierCurveTo(-32 + x, 2, 64 + x, 68, 15 + x, 70);
    ctx.stroke();
    //M 15 0 C 64 2 -32 68 15 70 M 15 0 C -32 2 64 68 15 70
}

function drawNine(x){
    ctx.beginPath();
    ctx.moveTo(22 + x, 39);
    ctx.bezierCurveTo(-49 + x, -15, 81 + x, -19, 4 + x, 70);
    ctx.stroke();
    //M 22 39 C -49 -15 81 -19 4 70
}

function drawColon(x){
    ctx.beginPath();
    ctx.moveTo(5 + x,23);
    ctx.bezierCurveTo(4 + x, 22, 6 + x, 24, 5 + x, 23)
    ctx.moveTo(5 + x, 46);
    ctx.bezierCurveTo(4 + x, 45, 6 + x, 47, 5 + x, 46)
    ctx.stroke();
}