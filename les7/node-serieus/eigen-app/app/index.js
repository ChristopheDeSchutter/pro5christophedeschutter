const hourConvert = require('hour-convert');
const location = {
    theaterzaal: 'theater zaal',
    kleineZaal: 'kleine zaal',
    foyer: 'foyer'
}

const coworkers ={
    johanDeQuick: 'Johan De Quick',
    johanVergauwen: 'Johan Vergauwen',
    christopheDeSchutter: 'Christophe De Schutter'
}

function showOverview(){
    return `Voorstelling ${this.name} staat in de ${this.location} met startuur ${this.startVoorstelling.hour}${this.startVoorstelling.meridiem}.`;
}

function building(){
    return `${this.name} word opgebouwd door ${this.coworker}. Start opbouw ${this.startUurBouw.hour}${this.startUurBouw.meridiem} en geschat einde ${this.eindeAfbraak.hour}${this.eindeAfbraak.meridiem}`;
}

let activity ={
    name:'De Koe',
    location: location.theaterzaal,
    coworker: [coworkers.johanDeQuick, coworkers.christopheDeSchutter],
    startUurBouw: hourConvert.to12Hour(11),
    startVoorstelling: hourConvert.to12Hour(20),
    eindeAfbraak: hourConvert.to12Hour(23),
    showOverview: showOverview,
    building: building
}

console.log(activity.showOverview());
console.log(activity.building());