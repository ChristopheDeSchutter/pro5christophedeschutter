const classGroups = {
    EA1: 'EA1',
    EA2: 'EA2',
    EA3: 'EA3'
}

function overallMark(){
    return (this.markProgrammingPrinciples + this.markCommunication + this.markWebTechnology)/3
}

function showOverview(){
    return `Naam: ${this.name.toLowerCase()}
    klasgroep: ${this.classGroup}
    Leeftijd: ${this.age}
    punten
        Communicatie: ${this.markCommunication}
        PP: ${this.markProgrammingPrinciples}
        Webtechnology: ${this.markWebTechnology}
        Gemiddelde: ${this.overallMark()}`;
}

let student = {
    name: 'Sareh El Farisi',
    age: 24,
    classGroup: classGroups.EA1,
    markCommunication: 13,
    markProgrammingPrinciples: 17,
    markWebTechnology: 19,
    overallMark: overallMark,
    showOverview: showOverview
}

let student2 = {
    name: 'Christophe De Schutter',
    age: '35',
    classGroup: classGroups.EA2,
    markCommunication: 13,
    markProgrammingPrinciples: 17,
    markWebTechnology: 19,
    overallMark: overallMark,
    showOverview: showOverview
};

console.log(student.showOverview());
console.log(student2.showOverview());
