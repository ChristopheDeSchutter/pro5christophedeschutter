let Student = {
    Name:'Christophe De Schutter',
    Age: 35,
    showOverview: function(){
        return `Mijn naam is ${this.Name} en ik ben ${this.Age} oud`;
    }
}

console.log(Student.showOverview());