const classGroups = {
    EA1: 'EA1',
    EA2: 'EA2',
    EA3: 'EA3'
}

let student = {
    name: 'Christophe De Schutter',
    age: '35',
    classGroup: classGroups.EA2,
    markCommunication: 13,
    markProgrammingPrinciples: 17,
    markWebTechnology: 19,
    overallMark: function(){
        return (this.markProgrammingPrinciples + this.markCommunication + this.markWebTechnology)/3
    },
    showOverview: function(){
        return `Naam: ${this.name}
        klasgroep: ${this.classGroup}
        Leeftijd: ${this.age}
        punten
            Communicatie: ${this.markCommunication}
            PP: ${this.markProgrammingPrinciples}
            Webtechnology: ${this.markWebTechnology}
            Gemiddelde: ${this.overallMark()}`;
    }
};

console.log(Student.showOverview());